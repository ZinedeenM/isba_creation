<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210227121812 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parameter ADD content_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter ADD CONSTRAINT FK_2A97911084A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('CREATE INDEX IDX_2A97911084A0A3ED ON parameter (content_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parameter DROP FOREIGN KEY FK_2A97911084A0A3ED');
        $this->addSql('DROP INDEX IDX_2A97911084A0A3ED ON parameter');
        $this->addSql('ALTER TABLE parameter DROP content_id');
    }
}
