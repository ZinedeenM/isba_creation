<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218001206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE container (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE container_content (container_id INT NOT NULL, content_id INT NOT NULL, INDEX IDX_60BDB4E4BC21F742 (container_id), INDEX IDX_60BDB4E484A0A3ED (content_id), PRIMARY KEY(container_id, content_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, content_id INT NOT NULL, image VARCHAR(1023) NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_C53D045F84A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, container_id INT DEFAULT NULL, label VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_140AB620BC21F742 (container_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE container_content ADD CONSTRAINT FK_60BDB4E4BC21F742 FOREIGN KEY (container_id) REFERENCES container (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE container_content ADD CONSTRAINT FK_60BDB4E484A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620BC21F742 FOREIGN KEY (container_id) REFERENCES container (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE container_content DROP FOREIGN KEY FK_60BDB4E4BC21F742');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB620BC21F742');
        $this->addSql('ALTER TABLE container_content DROP FOREIGN KEY FK_60BDB4E484A0A3ED');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F84A0A3ED');
        $this->addSql('DROP TABLE container');
        $this->addSql('DROP TABLE container_content');
        $this->addSql('DROP TABLE content');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE page');
    }
}
