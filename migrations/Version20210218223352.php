<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218223352 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE container ADD parent_container_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE container ADD CONSTRAINT FK_C7A2EC1BEC4FAA6C FOREIGN KEY (parent_container_id) REFERENCES container (id)');
        $this->addSql('CREATE INDEX IDX_C7A2EC1BEC4FAA6C ON container (parent_container_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE container DROP FOREIGN KEY FK_C7A2EC1BEC4FAA6C');
        $this->addSql('DROP INDEX IDX_C7A2EC1BEC4FAA6C ON container');
        $this->addSql('ALTER TABLE container DROP parent_container_id');
    }
}
