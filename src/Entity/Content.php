<?php

namespace App\Entity;

use App\Repository\ContentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContentRepository::class)
 */
class Content implements \ArrayAccess
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Container::class, mappedBy="contents")
     */
    private $containers;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="content", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="contents")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Parameter::class, mappedBy="content", cascade={"persist", "remove"})
     */
    private $parameters;

    private $param = [];

    public function __construct()
    {
        $this->containers = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->parameters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Container[]
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(Container $container): self
    {
        if (!$this->containers->contains($container)) {
            $this->containers[] = $container;
            $container->addContent($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): self
    {
        if ($this->containers->removeElement($container)) {
            $container->removeContent($this);
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setContent($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getContent() === $this) {
                $image->setContent(null);
            }
        }

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Parameter[]
     */
    public function getParameters(): Collection
    {
        return $this->parameters;
    }

    public function addParameter(Parameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $this->parameters[] = $parameter;
            $parameter->setContent($this);
        }

        return $this;
    }

    public function removeParameter(Parameter $parameter): self
    {
        if ($this->parameters->removeElement($parameter)) {
            // set the owning side to null (unless already changed)
            if ($parameter->getContent() === $this) {
                $parameter->setContent(null);
            }
        }

        return $this;
    }

    public function getParam(){
        if (empty($this->param)){
            $this->param = $this->parameters->getValues();
        }
        return $this->param;
    }

    public function offsetExists($offset)
    {
        foreach ($this->getParam() as $param){
            if (strtolower($param->getLabel()) === $offset){
                return true;
            }
        }
        return false;
    }

    public function offsetGet($offset)
    {
        foreach ($this->getParam() as $param){
            if (strtolower($param->getLabel()) === $offset){
                return $param->getValue();
            }
        }
        return null;
    }

    public function offsetSet($offset, $value)
    {
        $is_set = false;
        foreach ($this->getParam() as $param){
            if (strtolower($param->getLabel()) === $offset){
                $param->setValue($value);
                $is_set = true;
            }
        }
    }

    public function offsetUnset($offset)
    {

    }
}
