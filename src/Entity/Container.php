<?php

namespace App\Entity;

use App\Repository\ContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContainerRepository::class)
 */
class Container
{
    public const JUSTIFIED_CONTAINER = "justified_container";
    public const IMAGE = "image";

    public const LIST_COMPONENT = [
        "Conteneur justifié" => self::JUSTIFIED_CONTAINER,
        "Image" => self::IMAGE,
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Page::class, mappedBy="container", cascade={"persist", "remove"})
     */
    private $page;

    /**
     * @ORM\ManyToMany(targetEntity=Content::class, inversedBy="containers")
     */
    private $contents;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $component;

    /**
     * @ORM\ManyToOne(targetEntity=Container::class, inversedBy="containers")
     */
    private $parentContainer;

    /**
     * @ORM\OneToMany(targetEntity=Container::class, mappedBy="parentContainer")
     */
    private $containers;

    /**
     * @ORM\OneToMany(targetEntity=Parameter::class, mappedBy="container", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $parameters;

    public function __construct()
    {
        $this->contents = new ArrayCollection();
        $this->containers = new ArrayCollection();
        $this->parameters = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->label;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        // unset the owning side of the relation if necessary
        if ($page === null && $this->page !== null) {
            $this->page->setContainer(null);
        }

        // set the owning side of the relation if necessary
        if ($page !== null && $page->getContainer() !== $this) {
            $page->setContainer($this);
        }

        $this->page = $page;

        return $this;
    }

    /**
     * @return Collection|Content[]
     */
    public function getContents($randNum=null, $from=0, $limit=10)
    {

        if ($randNum){
            $array = $this->contents->getValues();
            mt_srand($randNum);
            $order = array_map(create_function('$val', 'return mt_rand();'), range(1, count($array)));
            array_multisort($order, $array);

            return array_slice($array, $from*$limit, $limit);
        }
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        $this->contents->removeElement($content);

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getComponent(): ?string
    {
        return $this->component;
    }

    public function setComponent(string $component): self
    {
        $this->component = $component;

        return $this;
    }

    public function getParentContainer(): ?self
    {
        return $this->parentContainer;
    }

    public function setParentContainer(?self $parentContainer): self
    {
        $this->parentContainer = $parentContainer;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(self $container): self
    {
        if (!$this->containers->contains($container)) {
            $this->containers[] = $container;
            $container->setParentContainer($this);
        }

        return $this;
    }

    public function removeContainer(self $container): self
    {
        if ($this->containers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getParentContainer() === $this) {
                $container->setParentContainer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Parameter[]
     */
    public function getParameters(): Collection
    {
        return $this->parameters;
    }

    public function addParameter(Parameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $this->parameters[] = $parameter;
            $parameter->setContainer($this);
        }

        return $this;
    }

    public function removeParameter(Parameter $parameter): self
    {
        if ($this->parameters->removeElement($parameter)) {
            // set the owning side to null (unless already changed)
            if ($parameter->getContainer() === $this) {
                $parameter->setContainer(null);
            }
        }

        return $this;
    }
}
