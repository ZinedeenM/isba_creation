<?php

namespace App\Controller\Admin;

use App\Entity\Container;
use App\Entity\Type;
use App\Form\ParameterType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ContainerCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Container::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $choices = [];
        foreach ($this->getDoctrine()->getRepository(Type::class)->findBy(['entityRelated' => Container::class])
             as
            $type
        ){
            $choices[$type->getType()] = $type->getType();
        }
        return [
            IdField::new('id')
                ->hideOnForm()
            ,
            TextField::new('label', 'Label')

            ,
            ChoiceField::new('component', 'Composant graphique')
                ->setChoices($choices)
            ,
            AssociationField::new('parentContainer', 'Container Parent (vide s\'il n\'y en a pas)'),
            CollectionField::new('parameters')
                ->setEntryType(ParameterType::class)
                ->setEntryIsComplex(true)
                ->setFormTypeOptions([
                    'by_reference' => 'false',
                ])
            ,
        ];
    }
}
