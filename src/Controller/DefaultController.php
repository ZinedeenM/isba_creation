<?php

namespace App\Controller;

use App\Entity\Container;
use App\Entity\Page;
use App\Services\ContainerService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(EntityManagerInterface $em): Response
    {
        $page = $em->getRepository(Page::class)->findOneBy(['label' => 'index']);
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'page' => $page,
            'random' => mt_rand(),
        ]);
    }

    /**
     * @Route("/image/{container}/{random}/{nb}", name="image")
     */
    public function image(Container $container, $random, $nb, ContainerService $containerService)
    {

        dump($containerService->getContainers($container, $random, $nb));

        return new Response($containerService->getContainers($container, $random, $nb));
    }
}
