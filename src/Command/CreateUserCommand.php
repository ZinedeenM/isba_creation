<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';

     private $passwordEncoder;
     private $em;

     public function __construct(string $name = null, UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
     {
         $this->passwordEncoder = $encoder;
         $this->em = $em;
         parent::__construct($name);
     }

    protected function configure()
    {
        $this->addArgument('password', InputArgument::REQUIRED, 'User password');
        $this->addArgument('user', InputArgument::REQUIRED, 'Username');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $user = new User();

        $user->setPassword($input->getArgument('password'));
        $user->setUsername($input->getArgument('user'));

        $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $input->getArgument('password')
        ));

        $this->em->persist($user);
        $this->em->flush();
        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return 0;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;
    }
}
