<?php


namespace App\Services;


use App\Entity\Container;
use App\Entity\Content;
use App\Entity\Image;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;

class ContainerService
{
    private $twig;
    private $em;

    public function __construct(Environment $twig, EntityManagerInterface $em)
    {
        $this->twig = $twig;
        $this->em = $em;
    }

    public function getContainers(?Container $container, $rand=null, $nb=null): string
    {
        $template = "";
        switch ($container? $container->getComponent() : "") {
            case Container::JUSTIFIED_CONTAINER:
                $template = $this->twig->render("component/justified_container.html.twig", [
                    'containers' => $container->getContainers()->getValues(),
                    'random' => $rand,
                ]);
                break;
            case Container::IMAGE:
                $template = $this->twig->render("component/image.html.twig", [
                    'contents' => $container->getContents($rand, $nb),
                    'random' => $rand,
                    'containerId' => $container->getId(),
                ]);
                break;
            default:
                break;
        }

        return $template;
    }

    public function flushImage()
    {
        /** @var Content $content */
        foreach ($this->em->getRepository(Image::class)->findBy(['content' => null]) as $content){
            $this->em->remove($content);
        }
    }
}