<?php

namespace App\Twig;

use App\Entity\Container;
use App\Services\ContainerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ContainerExtension extends AbstractExtension
{
    private $containerService;
    private $params;

    public function __construct(ContainerService $containerService, ParameterBagInterface $params)
    {
        $this->containerService = $containerService;
        $this->params = $params;
    }

    public function getFunctions(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFunction('getContainers', [$this, 'getContainers']),
            new TwigFunction('backDump', [$this, 'backDump']),
            new TwigFunction('getImage', [$this, 'getImage']),
        ];
    }

    public function getContainers(?Container $container, $rand=null):string
    {
        return $this->containerService->getContainers($container, $rand);
    }

    public function getImage($img){
        return file_exists($this->params->get('kernel.project_dir').'/public/'.$img) ? $img : 'image/noImage.png';
    }

    public function backDump($var){
        dump($var);
    }
}
