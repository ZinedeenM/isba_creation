import { Controller } from 'stimulus';
import Masonry from "masonry-layout";

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * masonry_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        $(window).scroll(function (){
            let height = $(window).scrollTop() + $(window).height();


            if (height > $('.ajax-loading').position().top && ($('.card-container').data('loading') === "oui")){
                $('.card-container').data('loading', 'non')
                $.ajax({
                    url : '/image/'+$('.card-container').data('container')+'/'+$('.card-container').data('random')+
                        '/'+$('.card-container').data('nbload'),
                    success : (res) =>{
                        $('.card-container').data('nbload', $('.card-container').data('nbload')+1)
                        $(res).find('.card-image').each(function ($key, $node){
                            $('.card-container').append($node)
                        });
                        $('.card-container').data('loading', 'oui')
                        // let mason = new Masonry('.card-container', {
                        //     itemSelector: '.card-image',
                        //     gutter: 20,
                        //     columnWidth: '.card-image-width',
                        //     fitWidth: true,
                        // });
                    }
                })
            }
        })
    }
}
