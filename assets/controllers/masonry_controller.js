import { Controller } from 'stimulus';
import Masonry from 'masonry-layout/masonry'

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * masonry_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        // let mason = new Masonry('.card-container', {
        //     itemSelector: '.card-image',
        //     gutter: 20,
        // });
    }
}
