import { Controller } from 'stimulus';
import Masonry from 'masonry-layout/masonry'

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * masonry_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        $('.card-image').click(function (e){
            let target = e.currentTarget;
            e.stopPropagation();

            $(target).find('.card-image-description').css('display', 'inline');
            $(target).find('.card-image-container').attr('class', 'doubling two column row card-image-container');
            $(target).find('span.circle').addClass('cross').click(function (e){
                e.stopPropagation();
                let target = $(e.currentTarget).parents('.card-image');

                $(target).find('.card-image-description').css('display', 'none');
                $(target).find('.card-image-container').attr('class', 'doubling one column row card-image-container');
                $(target).find('span.circle').removeClass('cross');
            });

        })
    }
}
